import './App.css';
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';

import {
  Table,
  Container,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  FormGroup,
  Label,
  Input,
} from 'reactstrap';

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      modalActualizar: false,
      modalInsertar: false,
      form: {
        id: "",
        name: "",
        abilities: "",
        types: "",
        moves: "",
        height: "",
        weight: "",
        artwork: "",
      },
    };
  }

  componentDidMount() {
    axios.get('https://pokeapi.co/api/v2/pokemon')
      .then(response => {
        const pokemonList = response.data.results.map(pokemon => ({
          id: pokemon.url.split('/')[6],
          name: pokemon.name,
        }));

        //Solicitud individual para cada pokm
        const promises = pokemonList.map(pokemon =>
          axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemon.id}`)
            .then(response => {
              //Datos especificos de pokemon
              pokemon.abilities = response.data.abilities.map(ability => ability.ability.name);
              pokemon.types = response.data.types.map(type => type.type.name);
              pokemon.moves = response.data.moves.splice(0, 12).map(move => move.move.name);
              pokemon.height = response.data.height;
              pokemon.weight = response.data.weight;
              pokemon.artwork = response.data.sprites.front_default;
            })
            .catch(error => {
              console.log(error);
            })
        );

        // Esperar a que se completen todas las solicitudes antes de actualizar el estado
        Promise.all(promises)
          .then(() => {
            this.setState({ data: pokemonList });
          });
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleChange = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  }

  delete = (dato) => {
    var opcion = window.confirm("¿Estás seguro que deseas eliminar este dato?" + dato.id);
    if (opcion === true) {
      var contador = 0;
      var arreglo = this.state.data;
      arreglo.forEach((registro, index) => {
        if (dato.id === registro.id) {
          arreglo.splice(index, 1);
        }
      });
      this.setState({ data: arreglo, modalActualizar: false });
    }
  }

  update = (char) => {
    var cont = 0;
    var newdata = this.state.data;
    newdata.forEach((registro, index) => {
      if (char.id === registro.id) {
        newdata[index].name = char.name;
        newdata[index].abilities = char.abilities;
        newdata[index].types = char.types;
        newdata[index].moves = char.moves;
        newdata[index].height = char.height;
        newdata[index].weight = char.weight;
        newdata[index].artwork = char.artwork;
      }
    });
    this.setState({ modalActualizar: false, data: newdata });
  }

  insert = (dato) =>{
    var newchar = {...this.state.form}
    newchar.id = this.state.data.length + 1
    var newdata = this.state.data
    newdata.push(newchar)
    this.setState({modalInsertar:false, data:newdata})
  }
  
  

  mostrarModalInsertar = () => {
    this.setState({ modalInsertar: true });
  }

  mostrarModalActualizar = (registro) => {
    this.setState({ modalActualizar: true, form: registro });
  }

  cerrarModalActualizar = () => {
    this.setState({ modalActualizar: false });
  }

  cerrarModalInsert = () => {
    this.setState({ modalInsertar: false });
  }

  render() {
    return (
      <div className='App bg bg-secondary'>
        <br></br>
        <br></br>
        <Container>
          <Button color='btn btn-success' onClick={this.mostrarModalInsertar}> New character </Button>
          <br></br>
          <hr></hr>
          <Table className='table table-dark'>
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Abilities</th>
                <th>Types</th>
                <th>Moves</th>
                <th>Height</th>
                <th>Weight</th>
                <th>Artwork</th>
                <th></th>
                <th>ACTIONS</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {this.state.data.map(pokemon => (
                <tr key={pokemon.id}>
                  <th>{pokemon.id}</th>
                  <th>{pokemon.name}</th>
                  <th>{pokemon.abilities}</th>
                  <th>{pokemon.types}</th>
                  <th>{pokemon.moves}</th>
                  <th>{pokemon.height}</th>
                  <th>{pokemon.weight}</th>
                  <th><img src={pokemon.artwork} alt={pokemon.name} height='200px' width='200px'></img></th>
                  <th><Button color='btn btn-primary'>Detail</Button></th>
                  <th><Button color='btn btn-info' onClick={() => this.mostrarModalActualizar(pokemon)}>Update</Button></th>
                  <th><Button color='btn btn-danger' onClick={() => this.delete(pokemon)}>Delete</Button></th>
                </tr>
              ))}
            </tbody>
          </Table>
        </Container>

        {/* Ventana modal para actualizar Pkm */}
        <Modal isOpen={this.state.modalActualizar}>
          <ModalHeader>
            <div>
              <h3>Actualizar monito</h3>
            </div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <input className='form-control' name='id' readOnly type='hidden' value={this.state.form.id}></input>
            </FormGroup>
            <FormGroup>
              <label>Name</label>
              <input className='form-control' name='name' type='text' onChange={this.handleChange} value={this.state.form.name}></input>
            </FormGroup>
            <FormGroup>
              <label>Abilities</label>
              <input className='form-control' name='abilities' type='text' onChange={this.handleChange} value={this.state.form.abilities}></input>
            </FormGroup>
            <FormGroup>
              <label>Types</label>
              <input className='form-control' name='types' type='text' onChange={this.handleChange} value={this.state.form.types}></input>
            </FormGroup>
            <FormGroup>
              <label>Moves</label>
              <input className='form-control' name='moves' type='text' onChange={this.handleChange} value={this.state.form.moves}></input>
            </FormGroup>
            <FormGroup>
              <label>Height</label>
              <input className='form-control' name='height' type='text' onChange={this.handleChange} value={this.state.form.height}></input>
            </FormGroup>
            <FormGroup>
              <label>Weight</label>
              <input className='form-control' name='weight' type='text' onChange={this.handleChange} value={this.state.form.weight}></input>
            </FormGroup>
            <FormGroup>
              <label>Artwork</label>
              <input className='form-control' name='artwork' type='text' onChange={this.handleChange} value={this.state.form.artwork}></input>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color='outline-warning' onClick={() => this.update(this.state.form)}>Update</Button>
            <Button color='outline-danger' onClick={() => this.cerrarModalActualizar()}>Cerrar</Button>
          </ModalFooter>
        </Modal>
        {/* Fin de modal edit */}

        {/* Ventana modal para insertar nuevos Pkm */}
        <Modal isOpen={this.state.modalInsertar}>
          <ModalHeader>
            <div>
              <h3>Agregar monito</h3>
            </div>
          </ModalHeader>
          <ModalBody>
            <FormGroup>
              <input className='form-control' name='id' type='hidden' value={this.state.data.length + 1}></input>
            </FormGroup>
            <FormGroup>
              <label>Name</label>
              <input className='form-control' name='name' type='text' onChange={this.handleChange}></input>
            </FormGroup>
            <FormGroup>
              <label>Abilities</label>
              <input className='form-control' name='abilities' type='text' onChange={this.handleChange}></input>
            </FormGroup>
            <FormGroup>
              <label>Types</label>
              <input className='form-control' name='types' type='text' onChange={this.handleChange}></input>
            </FormGroup>
            <FormGroup>
              <label>Moves</label>
              <input className='form-control' name='moves' type='text' onChange={this.handleChange}></input>
            </FormGroup>
            <FormGroup>
              <label>Height</label>
              <input className='form-control' name='height' type='text' onChange={this.handleChange}></input>
            </FormGroup>
            <FormGroup>
              <label>Weight</label>
              <input className='form-control' name='weight' type='text' onChange={this.handleChange}></input>
            </FormGroup>
            <FormGroup>
              <label>Artwork</label>
              <input className='form-control' name='artwork' type='text' onChange={this.handleChange}></input>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color='outline-warning' onClick={() => this.insert(this.state.form)}>Insert</Button>
            <Button color='outline-danger' onClick={() => this.cerrarModalInsert()}>Cerrar</Button>
          </ModalFooter>
        </Modal>
        {/* Fin de modal insert */}
      </div>
    );
  }
}